# Exercice 1 - Newsletter

##
- HTML
- CSS

## Architecture
- index.html
- readme.md
 - images
    - image-1.PNG
    - image-2.PNG
    - image-3.PNG
    - logo-proweb31.PNG
    - maquette-newsltter.PNG

## Consignes
- Réaliser une newsletter selon une maquette 
- Reproduire la newsletter avec les vidéos de la formatrice
- Réaliser la newsletter avec le système des tableaux en html/css